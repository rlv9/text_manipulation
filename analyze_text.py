import numpy as np

def open_file(filename):
    with open(filename, 'r') as fid:
        file1 = fid.readline()
        file2 = fid.readlines()
    return file2

def remove_newlines(list_name):
    for i, item in enumerate(list_name):
        list_name[i] = item.rstrip('\n')
    return list_name

def count_men(list_name):
    the_men = 0
    for i, item in enumerate(list_name):
        the_men += item.count('men')
        the_men += item.count('Men')
    return the_men

def capitalize_women(list_name):
    for i, item in enumerate(list_name):
        list_name[i] = item.replace('women', 'WOMEN')
        list_name[i] = item.replace('Women', 'WOMEN')
    return list_name

f = open_file('test.txt')
newfile = remove_newlines(f)
finalfile = capitalize_women(newfile)
print(finalfile)








